### github仓库
项目地址：https://github.com/tintinng/echarts-issue-helper
演示地址: https://tintinng.github.io/echarts-issue-helper/index.html
### 已完成功能
#### Markdown预览高亮
 - 表单preview后通用语法高亮、以及代码的高亮和github上类似
#### 支持图片上传
 - focus以下文本框表单，点击下方Attach image可获取返回的资源路径并转成markdown格式
   - BugReport\Steps to reproduce
   - BugReport\What is expected?
   - BugReport\What is actually happening?
   - BugReport\Any additional comments? (optional)
   - FeatureRequest\What problem does this feature solve?
   - FeatureRequest\What does the proposed API look like?
### 二期计划
- Markdown的表单输入实时高亮
- Markdown增加斜体\粗体的选中设置等